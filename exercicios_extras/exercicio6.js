function calculoFinancas(financas){
    let receita = 0
    let despesa = 0

    for(let i of financas.receitas) receita += i
    for(let j of financas.despesas) despesa += j

    console.log(`O saldo é ${(receita >= despesa) ? "positivo" : "negativo"}, valor: ${receita - despesa}`)
}

const financas = {
    receitas: [10, 20],
    despesas: [1, 1]
}

calculoFinancas(financas)