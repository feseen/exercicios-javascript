const valores = [3, 4, 2, 1, 5]
const copia = [...valores]

copia.sort((a, b) => {
    if(a > b) return 1
    if(a < b) return -1
    return 0
})

console.log(copia)
console.log("")
console.log(valores)