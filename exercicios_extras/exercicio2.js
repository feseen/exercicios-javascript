function recurssive(x, n){
    //mutiplicação por zero
    if(x == 0 || n == 0){
        return 0
    }
    //caso onde um dos números é negativo
    if(n < 0){
        x *= -1
    }

    //chamada recurssiva para o somatório
    if(Math.abs(n) > 1){
        x += recurssive(x, Math.abs(n) - 1)
    }
    return x
}

console.log(recurssive(16, -2))