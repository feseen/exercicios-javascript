function transformaNotas(notas){
    for(let i in notas){
        if(notas[i] > 90) notas[i] = 'A'
        else if(notas[i] >= 80) notas[i] = 'B'
        else if(notas[i] >= 70) notas[i] = 'C'
        else if(notas[i] >= 60) notas[i] = 'D'
        else notas[i] = 'F'
    }
}

const notas = [80.90, 100, 40, 60, 50.9, 80.0, 90.1, 70]
transformaNotas(notas)

console.log(notas)