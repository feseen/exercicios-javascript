const booksByCategory = [
    {
    category: "Riqueza",
    books: [
    {
    title: "Os segredos da mente milionária",
    author: "T. Harv Eker",
    },
    {
    title: "O homem mais rico da Babilônia",
    author: "George S. Clason",
    },
    {
    title: "Pai rico, pai pobre",
    author: "Robert T. Kiyosaki e Sharon L. Lechter",
    },
    ],
    },
    {
    category: "Inteligência Emocional",
    books: [
    {
    title: "Você é Insubstituível",
    author: "Augusto Cury",
    },
    {
    title: "Ansiedade – Como enfrentar o mal do século",
    author: "Augusto Cury",},
    {
    title: "Os 7 hábitos das pessoas altamente eficazes",
    author: "Stephen R. Covey"
    }
    ]
    }
    ]

//questão 1
const contarCategorias = (lista) => lista.length

const contarLivrosPorCategoria = (categoria) => categoria.books.length

const printCategoriasELivros = (lista) => {
    console.log("Num de categorias: ", contarCategorias(booksByCategory))
    lista.forEach((categoria) => console.log("Num de livros na categoria", categoria.category, ":", contarLivrosPorCategoria(categoria)))
}

printCategoriasELivros(booksByCategory)

//questão 2
const numDeAutores = (lista) => {
    let autores = []
    for(let categoria of lista){
        for(let livro of categoria.books){
            if(!autores.includes(livro.author)) autores.push(livro.author)
        }
    }
    return autores.length
}

console.log("Num de autores:", numDeAutores(booksByCategory))

//questão 3 e 4
const livrosDoAutor = (lista, nome) => {
    let livros = []
    for(let categoria of lista){
        for(let livro of categoria.books){
            if(livro.author == nome || livro.author.search(nome) != -1){
                livros.push(livro)
            }
        }
    }
    return livros
}

console.log(livrosDoAutor(booksByCategory, "Augusto Cury"))