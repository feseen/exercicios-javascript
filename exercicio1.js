let matA = [[2, -1], [2, 0]]
let matB = [[2, 3], [-2, 1]]

let multiply = function (matA, matB) {
    let matC = []
    for (let rowA = 0; rowA < matA.length; rowA++) {
        let arr = new Array(matB[0].length).fill(0)

        for (let colA = 0; colA < matA[0].length; colA++) {
            for (let i = 0; i < matB.length; i++) {
                arr[colA] += matA[rowA][i] * matB[i][colA]
            }
        }
        matC.push(arr)
    }
    return matC
}

console.log(multiply(matA, matB));