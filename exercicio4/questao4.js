import {gods} from "./arquivo_exercicio_4.js"

function getNamesAndClasses(gods){
    let godNamesAndClasses = []
    gods.forEach((god) => {
        godNamesAndClasses.push(god.name + " (" + god.class + ")")
    })
    return godNamesAndClasses
}

console.log(getNamesAndClasses(gods))